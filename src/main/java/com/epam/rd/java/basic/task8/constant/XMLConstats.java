package com.epam.rd.java.basic.task8.constant;

public enum XMLConstats {
    FLOWERS("flowers"), ATTRIBUTE("http://www.nure.ua"), FLOWER("flower"), NAME("name"), SOIL("soil"), ORIGIN("origin"),
    VISUALPARAMETERS("visualParameters"), STEAMCOLOUR("stemColour"), LEAFCOLOUR("leafColour"),
    AVELENFLOWER("aveLenFlower"), GROWINGTIPS("growingTips"), TEMPERETURE("tempreture"),
    LIGHTNING("lighting"), WATERING("watering"), MULTIPLYING("multiplying"),

    MEASURE("measure"),LIGHTREQUIRING("lightRequiring");

    private final String value;

    public String value() {
        return value;
    }

    XMLConstats(String value) {
        this.value = value.intern();
    }
}
