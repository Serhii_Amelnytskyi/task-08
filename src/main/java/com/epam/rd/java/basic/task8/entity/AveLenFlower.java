package com.epam.rd.java.basic.task8.entity;

import javax.xml.bind.annotation.*;
import java.math.BigInteger;
import java.util.Objects;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "value2"
})
public class AveLenFlower {

    @XmlValue
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger value2;
    @XmlAttribute(name = "measure", required = true)
    protected String measure2;

    public BigInteger getValue() {
        return value2;
    }

    public void setValue(BigInteger value) {
        this.value2 = value;
    }

    public String getMeasure() {
        return Objects.requireNonNullElse(this.measure2, "cm");
    }

    public void setMeasure(String value) {
        this.measure2 = value;
    }

}
