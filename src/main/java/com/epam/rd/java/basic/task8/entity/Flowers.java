package com.epam.rd.java.basic.task8.entity;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "flower"
})
@XmlRootElement(name = "flowers", namespace = "http://www.nure.ua")
public class Flowers {

    @XmlElement(namespace = "http://www.nure.ua")
    protected List<Flower> flower;


    public List<Flower> getFlower() {
        if (flower == null) {
            flower = new ArrayList<>();
        }
        return this.flower;
    }


}
