package com.epam.rd.java.basic.task8.entity;

import javax.xml.bind.annotation.*;
import java.math.BigInteger;
import java.util.Objects;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "value1"
})
public class Watering {

    @XmlValue
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger value1;
    @XmlAttribute(name = "measure", required = true)
    protected String measure1;

    public BigInteger getValue() {
        return value1;
    }

    public void setValue(BigInteger value) {
        this.value1 = value;
    }

    public String getMeasure() {
        return Objects.requireNonNullElse(measure1, "mlPerWeek");
    }

    public void setMeasure(String value) {
        this.measure1 = value;
    }

}
